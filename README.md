# By now, thprac has most of the features that this has
For the majority of cases, thprac is now a better tool than this SSG. However, due to how thprac works, having it attached to your game process makes the game much harder to debug with a proper debugger. This isn't a deliberate anti-debug measure (if it was, I would've removed it since I'm in charge of thprac now), it's simply how thprac works. You can check the [source code](https://github.com/touhouworldcup/thprac/) to verify. x64dbg and WinDbg have mechanisms to deal with it, Visual Studio does not.


## SSG for SpoilerAL ver 6.0 for Double-Dealing Character
### by 32th System



##### Credits:
Akaldar: for being the most awesome guy when it comes to getting me started on how to do this. Without him, this entire project would not have been possible

The creators of variuos ECL documentations. They have made possible, that I can understand ECL scripts and change them accordingly. An awesome ECL documentation is [this one](http://thwiki.cc/%E8%84%9A%E6%9C%AC%E5%AF%B9%E7%85%A7%E8%A1%A8/ECL). Bonus shoutoouts to Google Translate for making it readable for me.

Touhou Toolkit: for being able to make ECL scripts human readable and making it easier for me to find stuff in raw ECL scripts

(And most importantly) Cheat Engine: for being the most awesome debugger for analyzing proprietary software.

But I am also shouting out you, when you download, use and share your experiences with it.

### Usage of the Pracrice modifiers
- Enter the stage where you want to practice in "Practice Start" and pause immediatly
- Select your modifier
- Select "Give up and retry" in the pause menu. Select "Yes" to confirm.

### Issues and Pull requests
If you find a bug but don't have the technical knowlegde to fix it yourself, consider opening a new issue. If you know the fix to a bug, make a Pull request. All bug reports are appreciated and I will try to fix any bugs which I finf

### Changelog for version 1.2.1
Code locks added. You can now prevent the game from changing things like power and lifes. Barely tested, a 1.2.2 update will probably be required to fix potential bugs.

### Changelog for version 1.2
The Extra stage now has all the options all the other stages have. I used to think that MainSub04 runs during the entire span of the Extra stage after the MidBoss when really it only runs before any of the MainLatter subroutines kick in.

### Changelog for version 1.1.1
Whenever you use any option that changes an ECL script and is not the reset option, the stage logo will not show up.

### Changelog for vesion 1.1.0
Support for practicing Extra Stage Midboss nonspell as well as Raiko's nonspells added. Stage sections can not be practiced in the Extra Stage yet (knowing me it won't happen for a long time unless someone makes a ~~pull~~ merge request)

### Changelog for version 1.0.2
Added Cheat Table to the GitHub repository. I will try to keep this table up to date whenever I add a new bit of code or a new address to it.

### Changelog for version 1.0.1
You no longer have to multiply the PIV you want to give yourself on the fly with 100.
